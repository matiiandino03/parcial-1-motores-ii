﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Crate : MonoBehaviour
{
    public Door puertaScript;
    public Rigidbody2D rb;

    void Start()
    {
        puertaScript = FindObjectOfType<Door>();
        rb = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.CompareTag("Placa"))
        {
            puertaScript.Abrir();
        }
        else
        {
            puertaScript.Cerrar();
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.CompareTag("Placa"))
        {
            puertaScript.Abrir();
        }
        else
        {
            puertaScript.Cerrar();
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        puertaScript.Cerrar();
    }

    public void DesactivarRB()
    {
        
    }
}
