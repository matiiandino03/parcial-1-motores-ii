﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lever : MonoBehaviour
{
    public DoorLever puertaScript;
    public bool estaEnRango = false;
    public GameObject leverUp;
    public GameObject leverDown;

    void Start()
    {
        puertaScript = FindObjectOfType<DoorLever>();
    }
 
 
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.CompareTag("Leaver"))
        {
            estaEnRango = true;
        }
        else
        {
            estaEnRango = false;
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        estaEnRango = false;
    }

    public void Palanca()
    {
        if(estaEnRango)
        {
            puertaScript.Abrir();
            leverDown.SetActive(false);
            leverUp.SetActive(true);
        }
    }
}

