﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
    public GameObject MenuPrincipal;
    public GameObject Niveles;
    public GameObject ComoJugar;
    private void Awake()
    {
        MenuPrincipal.SetActive(true);
        Niveles.SetActive(false);
        ComoJugar.SetActive(false);
    }

    public void btNiveles()
    {
        MenuPrincipal.SetActive(false);
        Niveles.SetActive(true);
    }

    public void btComoJugar()
    {
        MenuPrincipal.SetActive(false);
        Niveles.SetActive(false);
        ComoJugar.SetActive(true);
    }

    public void btSalir()
    {
        Application.Quit();
    }

    public void btNivel1()
    {
        SceneManager.LoadScene("SwanSong");
    }
    public void btTutorial()
    {
        SceneManager.LoadScene("Tutorial");
    }

    public void btVolver()
    {
        MenuPrincipal.SetActive(true);
        Niveles.SetActive(false);
    }

    public void btVolverCJ()
    {
        ComoJugar.SetActive(false);
        MenuPrincipal.SetActive(true);
        Niveles.SetActive(false);
    }
}
