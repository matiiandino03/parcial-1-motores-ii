﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuPausa : MonoBehaviour
{
    public MovimientoJugador movimientoJugador;
    void Start()
    {
        movimientoJugador = FindObjectOfType<MovimientoJugador>();
    }

    public void Reanudar()
    {
        movimientoJugador.Reanudar();
    }

    public void Menu()
    {
        movimientoJugador.Menu();
    }

}
