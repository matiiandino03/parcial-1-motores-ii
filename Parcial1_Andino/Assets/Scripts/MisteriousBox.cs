﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MisteriousBox : MonoBehaviour
{
    public Transform puntoRespawn;
    public GameObject[] powerUps;
    int h;
    public GameObject CajaDesactivada;
    public GameObject thisGameObject;
    void SpawnPowerUp()
    {
        CajaDesactivada.SetActive(true);       
        Instantiate(powerUps[h], puntoRespawn.transform.position, puntoRespawn.transform.rotation);
        thisGameObject.SetActive(false);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        h = Random.Range(0, 4);
        if (collision.CompareTag("Player"))
        {
            SpawnPowerUp();
        }
    }
}
