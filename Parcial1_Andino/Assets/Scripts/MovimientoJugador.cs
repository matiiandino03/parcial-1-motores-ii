﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MovimientoJugador : MonoBehaviour
{
    public float runSpeed = 0;
    public float jumpSpeed = 0;
    Rigidbody2D rb;
    public bool isGrounded = false;
    public Text txtAccion;
    public bool dobleSalto = false;
    public bool estaEnAire = false;
    public Image barraO2;
    public float o2;
    public Text porcentajeVida;
    public bool puedeDobleSalto = false;
    public Transform CheckPoint1;
    public Transform CheckPoint2;
    public bool check1 = false;
    public bool check2 = false;
    public GameObject DS;
    bool win = false;
    public GameObject Victoria;
    public AudioSource audioSource;
    public Slider slider;
    public Text txtVolumen;
    public float volumen;
    public int volumeRounded;
    public GameObject menuPausa;
    public bool mirandoDerecha = true;


    private void Awake()
    {
        Time.timeScale = 1;
    }
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        InvokeRepeating("PerdidaO2", 0.5f, 0.5f);
        DS.SetActive(false);
        Victoria.SetActive(false);
        menuPausa.SetActive(false);
        audioSource.volume = PlayerPrefs.GetFloat("Volumen");
        slider.value = audioSource.volume;
    }

    private void Update()
    {
        o2 = Mathf.Clamp(o2, 0, 100);
        barraO2.fillAmount = o2 / 100;
        porcentajeVida.text = o2.ToString() + "%";
        if (dobleSalto)
        {
            DS.SetActive(true);
        }

        if (o2 < 0.0f || o2 == 0)
        {
            GuardarVolumen();
            SceneManager.LoadScene("SwanSong");
        }

        volumen = audioSource.volume * 100;
        volumeRounded = Mathf.RoundToInt(volumen);
        txtVolumen.text = "Volumen: " + volumeRounded.ToString() + "%"; ;
    }
    void FixedUpdate()
    {

        rb.velocity = new Vector2(runSpeed, rb.velocity.y);

    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        isGrounded = true;
        estaEnAire = false;
    }

    public void OnCollisionExit2D(Collision2D collision)
    {
        isGrounded = false;
    }

    public void MovimientoAdelante()
    {
        runSpeed = 4;
        if(!mirandoDerecha)
        {
            Girar();
        }
    }

    public void MovimientoAtras()
    {
        runSpeed = -4;
        if (mirandoDerecha)
        {
            Girar();
        }
    }

    public void Salto()
    {
        if (dobleSalto && puedeDobleSalto)
        {
            rb.velocity = new Vector2(rb.velocity.x, jumpSpeed);
            puedeDobleSalto = false;
        }
        else if (estaEnAire == false)
        {
            rb.velocity = new Vector2(rb.velocity.x, jumpSpeed);
            estaEnAire = true;
            puedeDobleSalto = true;
        }
    }

    public void Freno()
    {
        runSpeed = 0;
    }

    public void Pausa()
    {
        Time.timeScale = 0;
        menuPausa.SetActive(true);
    }

    public void Reanudar()
    {
        if (win == false)
        {
            Time.timeScale = 1;
            menuPausa.SetActive(false);
        }
    }

    public void Menu()
    {
        GuardarVolumen();
        SceneManager.LoadScene(0);
    }
    public void Reset()
    {
        GuardarVolumen();
        SceneManager.LoadScene("SwanSong");
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Respawn"))
        {
            Freno();
            if (check1 == true)
            {
                transform.position = CheckPoint1.position;
            }
            if (check2 == true)
            {
                transform.position = CheckPoint2.position;
            }
        }

        if (collision.CompareTag("Map"))
        {
            runSpeed = 0;
        }

        if (collision.CompareTag("O2"))
        {
            o2 += 50;
            Destroy(collision);
        }

        if (collision.CompareTag("piso"))
        {
            isGrounded = true;
        }

        if (collision.CompareTag("CheckPoint1"))
        {
            check1 = true;
        }
        if (collision.CompareTag("CheckPoint2"))
        {
            check1 = false;
            check2 = true;
        }

        if (collision.CompareTag("Finish"))
        {
            win = true;
            Time.timeScale = 0;
            Victoria.SetActive(true);
        }
    }

    void PerdidaO2()
    {
        o2 -= 1.0f;
    }

    public void CambiarVolumen()
    {
        audioSource.volume = slider.value;
    }

    public void BajarVolumenVoz()
    {
        slider.value = 0.0f;
    }

    void GuardarVolumen()
    {
        PlayerPrefs.SetFloat("Volumen", audioSource.volume);
    }

    void Girar()
    {
        mirandoDerecha = !mirandoDerecha;
        Vector3 escala = transform.localScale;
        escala.x *= -1;
        transform.localScale = escala;
    }
}
