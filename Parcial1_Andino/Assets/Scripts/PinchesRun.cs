﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PinchesRun : MonoBehaviour
{
    public float velocidad = 8f;

    void FixedUpdate()
    {
        transform.position += transform.right * velocidad * Time.fixedDeltaTime;
    }
}
