﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpDobleSalto : MonoBehaviour
{
    public MovimientoJugador jugadorScript;
    void Start()
    {
        jugadorScript = FindObjectOfType<MovimientoJugador>();
    }

    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.CompareTag("Player"))
        {
            jugadorScript.dobleSalto = true;
            Destroy(this.gameObject);
        }
    }
}
