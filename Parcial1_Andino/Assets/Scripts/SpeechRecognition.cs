﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Windows.Speech;
using System.Linq;
using UnityEngine.UI;

public class SpeechRecognition : MonoBehaviour
{

    public Text txtAccion;

    private KeywordRecognizer detectorVoz;
    private ConfidenceLevel confidencialidad = ConfidenceLevel.Low;
    private Dictionary<string, Accion> accionVoz = new Dictionary<string, Accion>();
    private MovimientoJugador movimientoJugador;
    private Lever lever;

    private delegate void Accion();

    void Start()
    {
        movimientoJugador = GetComponent<MovimientoJugador>();
        lever = GetComponent<Lever>();

        accionVoz.Add("Corre", movimientoJugador.MovimientoAdelante);
        accionVoz.Add("Atras", movimientoJugador.MovimientoAtras);
        accionVoz.Add("Salta", movimientoJugador.Salto);
        accionVoz.Add("Frena", movimientoJugador.Freno);
        accionVoz.Add("Reiniciar", movimientoJugador.Reset);
        accionVoz.Add("Usar", lever.Palanca);
        accionVoz.Add("Pausa", movimientoJugador.Pausa);
        accionVoz.Add("Reanudar", movimientoJugador.Reanudar);
        accionVoz.Add("Menu", movimientoJugador.Menu);
        accionVoz.Add("Mutear", movimientoJugador.BajarVolumenVoz);

        detectorVoz = new KeywordRecognizer(accionVoz.Keys.ToArray(), confidencialidad);
        detectorVoz.OnPhraseRecognized += OnKeywordsRecognized;
        detectorVoz.Start();
    }

    void OnDestroy()
    {
        if (detectorVoz != null && detectorVoz.IsRunning)
        {
            detectorVoz.Stop();
            detectorVoz.Dispose();
        }
    }

    private void OnKeywordsRecognized(PhraseRecognizedEventArgs args)
    {
        txtAccion.text = args.text;
        accionVoz[args.text].Invoke();
    }
}
