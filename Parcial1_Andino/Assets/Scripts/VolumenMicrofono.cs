﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VolumenMicrofono : MonoBehaviour
{
    public int sampleWindow = 64;
    private AudioClip clipMicrofono;

    public float sensibilidadVolumen = 1000;
    public float threshold = 1f;

    public Image VolumeBarra;
    public float volumenMicro;

    public Text txtMicro;

    void Start()
    {
        MicrofonoAudioClip();
    }
    void Update()
    {
        volumenMicro = MedirVolumenMicrofono(Microphone.GetPosition(Microphone.devices[0]), clipMicrofono)*sensibilidadVolumen;
        if(volumenMicro < threshold)
        {
            volumenMicro = 0;
        }
        volumenMicro = Mathf.Clamp(volumenMicro, 0, 100);
        for(int i = 0; i < 5; i++)
        VolumeBarra.fillAmount = volumenMicro / 100;
        txtMicro.text = Microphone.devices[0].ToString();
    }

    public void MicrofonoAudioClip()
    {
        string nombreMicrofono = Microphone.devices[0];
        clipMicrofono = Microphone.Start(nombreMicrofono, true, 20, AudioSettings.outputSampleRate);
    }


    public float MedirVolumenMicrofono(int posicionClip,AudioClip clip)
    {
        int posicionInicio = posicionClip - sampleWindow;
        float[] waveData = new float[sampleWindow];
        clip.GetData(waveData, posicionInicio);

        float volumen = 0;

        for(int i = 0; i< sampleWindow; i++)
        {
            volumen += Mathf.Abs(waveData[i]);
        }
        return volumen / sampleWindow;
    }
}
